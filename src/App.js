import logo from './logo.svg';
import './App.css';
import ShoeShop from './Shoe_Shop/ShoeShop';

function App() {
  return (
    <div className="App">
     <ShoeShop>Cyber Shoe Shop</ShoeShop>
    </div>
  );
}

export default App;

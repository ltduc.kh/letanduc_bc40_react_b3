import React, { Component } from 'react'
import Cart from './Cart'
import { dataShoe } from './dataShoe'
import ListShoe from './ListShoe'
export default class ShoeShop extends Component {
  state = {
    listShoe: dataShoe,
    cart: [],
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    
    //th1 : sp chưa có trong giỏ hàng => push 
    if (index == -1) {
      
      let newShoe = { ...shoe, soLuong: 1 };
       cloneCart.push(newShoe);
     } else {
      cloneCart[index].soLuong++;
    }
    //th2 : sp chưa có trong giỏ hàng => push 
    
    this.setState({
      cart: cloneCart,
    });

  };
  handleChangeQuantity = (id, soLuong) => {
    var cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex(item => item.id ===id);
    if(soLuong){
      cloneCart[index].soLuong += 1;
    }else{

      if(cloneCart[index].soLuong>1)
      {
        cloneCart[index].soLuong -= 1;
      }
    }
    //soLuong : 1 hoặc -1
    this.setState({
      cart: cloneCart
    })
  }
  
  handleRemoveShoe = (id) => {
    //index
    //splice(index,1)
   var newShoe = [...this.state.shoe];
   let index = newShoe.findIndex(item=> item.id == id);
   if(index !== -1) {
    newShoe.splice(index,1)
   }
   //cap nhat gio hang
   this.setState({
    shoe: newShoe
   })
  }
  render() {
    
    return (
      <div className='container'>
        <h2>{this.props.children}</h2>
        <Cart handleChangeQuantity={this.handleChangeQuantity} handleRemoveShoe={this.handleRemoveShoe}  cart={this.state.cart} />
        <ListShoe handleAddToCart={this.handleAddToCart} list={this.state.listShoe} />
      </div>
    )
  }
}
